package com.kblecher;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.ObjectifyService;

import static com.googlecode.objectify.ObjectifyService.ofy;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.IOException;
import java.util.List;


/**
 * Created by Kathryn on 1/31/2016.
 */
public class TictactoeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain;charset=UTF-8");

        String action = request.getParameter("game");

        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        String username = user.getNickname();
        Player p = ofy().load().type(Player.class).filter("username", username).first().now();

        if (action!=null && action.equals("win")) {
            p.highScore = p.highScore+1;
            ObjectifyService.ofy().save().entity(p).now();
        }

        if (action!=null && action.equals("lose")) {
            p.losses = p.losses+1;
            ObjectifyService.ofy().save().entity(p).now();
        }

        if (action!=null && action.equals("tie")) {
            p.ties = p.ties+1;
            ObjectifyService.ofy().save().entity(p).now();
        }

        List<Player> highScoreTable = ObjectifyService.ofy()
                .load()
                .type(Player.class)
                .order("-highScore")
                .limit(10)
                .list();
        String attr1, attr2, attr3, attr4;
        for (int i=0; i < 10; i++) {
            attr1 = "player" + (1 + i);
            attr2 = "score" + (1 + i);
            attr3 = "losses" + (1 + i);
            attr4 = "ties" + (1 + i);
            if (i < highScoreTable.size()) {
                request.setAttribute(attr1, highScoreTable.get(i).username);
                request.setAttribute(attr2, highScoreTable.get(i).highScore);
                request.setAttribute(attr3, highScoreTable.get(i).losses);
                request.setAttribute(attr4, highScoreTable.get(i).ties);
            }
            else {
                request.setAttribute(attr1, "null");
                request.setAttribute(attr2, "null");
                request.setAttribute(attr3, "null");
                request.setAttribute(attr4, "null");
            }
        }
        response.sendRedirect("/");
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        String loginUrl = userService.createLoginURL("/");
        String logoutUrl = userService.createLogoutURL("/");

        if (user!=null) {
            String username = user.getNickname();

            Player p = ObjectifyService.ofy().load().type(Player.class).filter("username", username).first().now();

            if (p == null) {
                Player pNew = new Player(username);
                req.setAttribute("highScore", pNew.highScore);
                ObjectifyService.ofy().save().entity(pNew).now();
            } else {
                req.setAttribute("highScore", p.highScore);
                req.setAttribute("turn", p.turn);
            }

            List<Player> highScoreTable = ObjectifyService.ofy()
                    .load()
                    .type(Player.class)
                    .order("-highScore")
                    .limit(10)
                    .list();
            String attr1, attr2, attr3, attr4;
            for (int i=0; i < 10; i++) {
                attr1 = "player" + (1 + i);
                attr2 = "score" + (1 + i);
                attr3 = "losses" + (1 + i);
                attr4 = "ties" + (1 + i);
                if (i < highScoreTable.size()) {
                    req.setAttribute(attr1, highScoreTable.get(i).username);
                    req.setAttribute(attr2, highScoreTable.get(i).highScore);
                    req.setAttribute(attr3, highScoreTable.get(i).losses);
                    req.setAttribute(attr4, highScoreTable.get(i).ties);
                }
                else {
                    req.setAttribute(attr1, "null");
                    req.setAttribute(attr2, "null");
                    req.setAttribute(attr3, "null");
                    req.setAttribute(attr4, "null");
                }
            }
        }

        req.setAttribute("user", user);
        req.setAttribute("loginUrl", loginUrl);
        req.setAttribute("logoutUrl", logoutUrl);

        resp.setContentType("text/html");

        RequestDispatcher jsp = req.getRequestDispatcher("/game.jsp");
        jsp.forward(req, resp);

    }

}



