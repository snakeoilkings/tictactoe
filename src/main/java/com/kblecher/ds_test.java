package com.kblecher;

/**
 * Created by Kathryn on 2/6/2016.

import com.google.appengine.api.datastore.*;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.FilterOperator;
import java.util.List;

public class ds_test {
    private String name;
    private int score;

    // Get the Datastore Service
    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();


    // lists top ten user score
    public List<Entity> topTen() {
        Query q = new Query("Player").addSort("highScore", Query.SortDirection.DESCENDING);

        PreparedQuery pq = datastore.prepare(q);

        return pq.asList(FetchOptions.Builder.withLimit(10));
    }

    // creates a new player and sets score to 0
    public Player(String username) {
        name = username;
        score = 0;
        addPlayer(username);
    }

    // creates a new player and sets score to highscore
    public Player(String username, int highScore) {
        name = username;
        score = highScore;
    }

    // adds player to the datastore
    public void addPlayer(String username) {
        Entity player = new Entity("Player");
        player.setProperty("username",username);
        player.setProperty("highScore", 0);
        datastore.put(player);
    }

    // gets the current player score
    public int getScore(String usern) {
        Query q = new Query("Person").setFilter(new FilterPredicate("username",
                FilterOperator.EQUAL, usern));
        if (q==null) {
            Player newPlayer = new Player(usern);
            return 0;
        }
        else {
            PreparedQuery pq = datastore.prepare(q);
            Entity result = pq.asSingleEntity();
            return (int) result.getProperty("highScore");
        }
    }

    public Player getPlayer(String usern) {
        Query q = new Query("Person").setFilter(new FilterPredicate("username",
                FilterOperator.EQUAL, usern));
        if (q==null) {
            Player newPlayer = new Player(usern);
            return newPlayer;
        }
        else {
            PreparedQuery pq = datastore.prepare(q);
            Entity result = pq.asSingleEntity();
            return (String) result.getProperty("username");
        }
    }

    // increments the player score after a win
    public void incScore(String usern) {
        Query q = new Query("Person").setFilter(new FilterPredicate("username",
                FilterOperator.EQUAL, usern));
        if (q==null) {
            //ERROR
        }
        else {
            PreparedQuery pq = datastore.prepare(q);
            Entity result = pq.asSingleEntity();
            int highScore = (int) result.getProperty("highScore");
            highScore++;
            result.setProperty("highScore", highScore);
            datastore.put(result);
        }

    }

}
**/