package com.kblecher;

import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import java.lang.String;

/**
 *
 **/
@Entity
public class Player {
    @Id public Long id;

    @Index public String username;
    @Index public int highScore;
    public int losses;
    public int ties;
    public boolean turn;

    /**
     * Simple constructor just sets high score to 0
     **/
    public Player() {
        highScore = 0;
        ties = 0;
        losses = 0;
        turn = true;
    }

    public Player(String username) {
        this.username = username;
        highScore = 0;
        losses = 0;
        ties = 0;
        turn=true;
    }
}