<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link href='https://fonts.googleapis.com/css?family=Fontdiner+Swanky|Josefin+Sans:600' rel='stylesheet' type='text/css'>


<html>
    <head>
        <title>Ultimate Tic-Tac-Toe</title>
        <link type="text/css" rel="stylesheet" href="/stylesheets/main.css"/>
        <script src="/scripts/jquery-2.2.0.min.js"></script>
        <script src = "/scripts/tictactoe.js"></script>
    </head>


    <body>
    <div id = "gameArea">
        <header align = "center">
            <h1 align = "center">Tic - Tac - Toe</h1>
        </header>
        <br>
        <c:choose>
            <c:when test="${user != null}">

            <p align = "center" id="loggedin">Hello, ${fn:escapeXml(user.nickname)}! (sign out
            <a href="${logoutUrl}">here</a>).</p>

            </c:when>
        <c:otherwise>
            <p align = "center" id="welcome">Welcome!<br><br>

             <button align = "center" id = "login"  onclick = "location.href='${loginUrl}'">Log in here to play! </button>
            </c:otherwise>
        </c:choose>

    <c:if test="${user != null}">
        <table id = "scoreTable" align="center">
            <tr>
                <td colspan="4">High Score Table</td>
            </tr>
            <tr>
                <td>Player</td>
                <td>Wins</td>
                <td>Losses</td>
                <td>Ties</td>
            </tr>
            <tr>
                <td> ${player1}</td>
                <td> ${score1}</td>
                <td> ${losses1}</td>
                <td> ${ties1}</td>
            </tr>
            <tr>
                <td> ${player2}</td>
                <td> ${score2}</td>
                <td> ${losses2}</td>
                <td> ${ties2}</td>
            </tr>
            <tr>
                <td> ${player3}</td>
                <td> ${score3}</td>
                <td> ${losses3}</td>
                <td> ${ties3}</td>
            </tr>
            <tr>
                <td> ${player4}</td>
                <td> ${score4}</td>
                <td> ${losses4}</td>
                <td> ${ties4}</td>
            </tr>
            <tr>
                <td> ${player5}</td>
                <td> ${score5}</td>
                <td> ${losses5}</td>
                <td> ${ties5}</td>
            </tr>
            <tr>
                <td> ${player6}</td>
                <td> ${score6}</td>
                <td> ${losses6}</td>
                <td> ${ties6}</td>
            </tr>
            <tr>
                <td> ${player7}</td>
                <td> ${score7}</td>
                <td> ${losses7}</td>
                <td> ${ties7}</td>
            </tr>
            <tr>
                <td> ${player8}</td>
                <td> ${score8}</td>
                <td> ${losses8}</td>
                <td> ${ties8}</td>
            </tr>
            <tr>
                <td> ${player9}</td>
                <td> ${score9}</td>
                <td> ${losses9}</td>
                <td> ${ties9}</td>
            </tr>
            <tr>
                <td> ${player10}</td>
                <td> ${score10}</td>
                <td> ${losses10}</td>
                <td> ${ties10}</td>
            </tr>


        </table>

        <br><br>
        <button align = "center" id = "startGame"  onclick = "drawBoard()">Start playing!</button>
            <canvas id = "gameBoard">
            </canvas>
        <br><button align = "center" id = "return" onclick = "refresh()">Go Back</button><br>
        </div>
    </body>
</c:if>



</body>


</body>
</html>