/**
 * Created by Kathryn on 1/31/2016.
 */
var moves = 0;       //checks to see whose turn it is, even = player, odd = computer
var empty_spaces = [0, 1, 2, 3, 4, 5, 6, 7,  8];
var x_spaces = [];
var o_spaces = [];
var array_pos;
var win = false;
var lose = false;
var tie = false;
var start = false;
var started = false;
var gameOver = false;
var playerTurn = true; // when true: player goes first, when false: computer goes first. Alternates each game


function resetGame() {
    moves = 0;
    empty_spaces = [0, 1, 2, 3, 4, 5, 6, 7,  8];
    x_spaces = [];
    o_spaces = [];
    win = false;
    lose = false;
    tie = false;
    start = false;
    started = false;
    gameOver = false;
    playerTurn = (playerTurn == true) ? false : true;
    drawText("click here to start");
}

function drawMove(num, player) {
    // find x position to draw move
    if (num==0 || num==3 || num==6) { x=0 }
    if (num==1 || num==4 || num==7) { x=200 }
    if (num==2 || num==5 || num==8) { x=400 }
    // find y position to draw move
    if (num==0 || num==1 || num==2) { y=0 }
    if (num==3 || num==4 || num==5) { y=200 }
    if (num==6 || num==7 || num==8) { y=400 }
    // draw an x if it's the player's move
    var c = document.getElementById("gameBoard");
    var ctx = c.getContext("2d");
    if (player == "x") {
        ctx.moveTo(x+30, y+30);
        ctx.lineTo(x+170,y+170);
        ctx.moveTo(x+170, y+30);
        ctx.lineTo(x+30,y+170);
    }
    //draw a o if it's the computers move
    if (player == "o"){
        ctx.beginPath();
        ctx.arc(x+100,y+100,60,0,2*Math.PI);
    }
    ctx.stroke();
}


function select(num) {
    if (moves % 2 == 0 && empty_spaces.indexOf(num) >= 0 && lose==false) {
        // place X on board
        drawMove(num, "x");
        // increase number of total moves placed
        moves++;
        // remove space from the available spaces list
        array_pos = empty_spaces.indexOf(num);
        // place the spot in the human-captured spaces
        empty_spaces.splice(array_pos, 1);
        x_spaces.push(num);
        // check to see if user won
        user_win_check();
        // if the user hasn't won, let computer take it's turn
        if (win == false && empty_spaces.length > 0) {
            var think = 700 + Math.floor((Math.random() * 500));
            setTimeout(computer_select, 800);
        }
        else if (win == false && empty_spaces.length < 1)
            tieScreen();
        // else, show the win screen
        else
            winScreen();
    }
}

function computer_select() {
    var o = 0;
    if (empty_spaces.length > 0) {
        while (moves % 2 != 0) {
            //pick a spot at random
            o = Math.floor((Math.random() * 9));
            if (empty_spaces.indexOf(o) >= 0)
                moves++;
        }
        drawMove(o, "o");
        array_pos = empty_spaces.indexOf(o);
        var r = empty_spaces[array_pos];
        o_spaces.push(r);
        empty_spaces.splice(array_pos, 1);

        comp_win_check();
        if (lose == true) {
            moves++;
            loseScreen();
        }
        else if (win == false && empty_spaces.length < 1)
            tieScreen();
    }
}

function user_win_check() {
    /* user wins */
    if (x_spaces.indexOf(0) >= 0 && x_spaces.indexOf(1) >= 0 && x_spaces.indexOf(2) >= 0) { win = true; }
    if (x_spaces.indexOf(3) >= 0 && x_spaces.indexOf(4) >= 0 && x_spaces.indexOf(5) >= 0) { win = true; }
    if (x_spaces.indexOf(6) >= 0 && x_spaces.indexOf(7) >= 0 && x_spaces.indexOf(8) >= 0) { win = true; }
    if (x_spaces.indexOf(0) >= 0 && x_spaces.indexOf(3) >= 0 && x_spaces.indexOf(6) >= 0) { win = true; }
    if (x_spaces.indexOf(1) >= 0 && x_spaces.indexOf(4) >= 0 && x_spaces.indexOf(7) >= 0) { win = true; }
    if (x_spaces.indexOf(2) >= 0 && x_spaces.indexOf(5) >= 0 && x_spaces.indexOf(8) >= 0) { win = true; }
    if (x_spaces.indexOf(0) >= 0 && x_spaces.indexOf(4) >= 0 && x_spaces.indexOf(8) >= 0) { win = true; }
    if (x_spaces.indexOf(2) >= 0 && x_spaces.indexOf(4) >= 0 && x_spaces.indexOf(6) >= 0) { win = true; }
}

function comp_win_check() {
    /* computer wins */
    if (o_spaces.indexOf(0) >= 0 && o_spaces.indexOf(1) >= 0 && o_spaces.indexOf(2) >= 0) { lose = true; }
    if (o_spaces.indexOf(3) >= 0 && o_spaces.indexOf(4) >= 0 && o_spaces.indexOf(5) >= 0) { lose = true; }
    if (o_spaces.indexOf(6) >= 0 && o_spaces.indexOf(7) >= 0 && o_spaces.indexOf(8) >= 0) { lose = true; }
    if (o_spaces.indexOf(0) >= 0 && o_spaces.indexOf(3) >= 0 && o_spaces.indexOf(6) >= 0) { lose = true; }
    if (o_spaces.indexOf(1) >= 0 && o_spaces.indexOf(4) >= 0 && o_spaces.indexOf(7) >= 0) { lose = true; }
    if (o_spaces.indexOf(2) >= 0 && o_spaces.indexOf(5) >= 0 && o_spaces.indexOf(8) >= 0) { lose = true; }
    if (o_spaces.indexOf(0) >= 0 && o_spaces.indexOf(4) >= 0 && o_spaces.indexOf(8) >= 0) { lose = true; }
    if (o_spaces.indexOf(2) >= 0 && o_spaces.indexOf(4) >= 0 && o_spaces.indexOf(6) >= 0) { lose = true; }
}

function winScreen() {
    $.ajax({
        type: "POST",
        url: "/",
        data: {game: "win"}
    });
    drawResult("you win!");
    setTimeout(endGame, 1000);
}

function tieScreen() {
    $.ajax({
        type: "POST",
        url: "/",
        data: {game: "tie"}
    });
    drawResult("it's a tie!");
    setTimeout(endGame, 1000);
}

function loseScreen() {
    $.ajax({
        type: "POST",
        url: "/",
        data: {game: "lose"}
    });
    drawResult("you lose");
    setTimeout(endGame, 1000);
}

function endGame() {
    gameOver = true;
}

function drawText(input) {
    var c = document.getElementById("gameBoard");
    var ctx = c.getContext("2d");
    ctx.clearRect(0, 0, c.width, c.height);
    ctx.beginPath();
    ctx.fillStyle = 'rgba(255,255,255,1.0)';
    ctx.fillRect(0, 0, 600, 600);
    ctx.fillStyle = 'rgba(0,0,0,1.0)';
    ctx.fillRect(0, 0, 600, 600);
    ctx.font = '22px Fontdiner Swanky';
    ctx.fillStyle = "white";
    ctx.textAlign = "center";
    ctx.fillText(input, c.width / 2, c.height / 2);
    ctx.closePath();
}

function refresh() {
    window.location.reload();
}

function drawBoard() {
    var c = document.getElementById("gameBoard");
    c.width="600";
    c.height="600";
    document.getElementById("startGame").style.display = "none";
    document.getElementById("scoreTable").style.display = "none";
    document.getElementById("return").style.display = "block";
    var ctx = c.getContext("2d");
    drawText("click here to start");

    c.addEventListener("click", function (event) {
        if (started == true) { return; }

        if (start == false) {
            started = true;
            drawText("5");
            setTimeout(drawText, 1000, "4");
            setTimeout(drawText, 2000, "3");
            setTimeout(drawText, 3000, "2");
            setTimeout(drawText, 4000, "1");
            setTimeout(draw,5000,ctx);
        }

        if (start == true) {
            var rec = c.getBoundingClientRect();
            var x = event.clientX - rec.left;
            var y = event.clientY - rec.top;

            var id = 0;

            if (x < 200 && y < 200) {id = 0;}
            if (x < 400 && x >= 200 && y < 200){id = 1;}
            if (x < 600 && x >= 400 && y < 200) {id = 2;}
            if (x < 200 && y < 400 && y >= 200) {id = 3;}
            if (x < 400 && x >= 200 && y < 400 && y >= 200) {id = 4;}
            if (x < 600 && x >= 400 && y < 400 && y >= 200) {id = 5;}
            if (x < 200 && y < 600 && y >= 400) {id = 6;}
            if (x < 400 && x >= 200 && y < 600 && y >= 400) {id = 7;}
            if (x < 600 && x >= 400 && y < 600 && y >= 400) {id = 8;}

            select(id);
        }

        if (gameOver==true) {
            gameOver = false;
            resetGame();
        }

   }, false);
}

function draw(ctx) {
    ctx.fillStyle = 'rgba(255,255,255,1.0)';
    ctx.fillRect(0, 0, 600, 600);
    ctx.strokeStyle = 'rgba(0,0,0,1.0)';
    ctx.strokeRect(0, 0, 600, 600);
    ctx.moveTo(200, 0);
    ctx.lineTo(200, 600);
    ctx.moveTo(400, 0);
    ctx.lineTo(400, 600);
    ctx.moveTo(0, 200);
    ctx.lineTo(600, 200);
    ctx.moveTo(0, 400);
    ctx.lineTo(600, 400);
    ctx.lineWidth = 5;
    ctx.stroke();
    start = true;
    started = false;

    //check to see if computer goes first
    if (playerTurn == false) {
        moves++;
        computer_select();
    }
}

function drawResult(result) {
    var c = document.getElementById("gameBoard");
    var ctx = c.getContext("2d");
    ctx.beginPath();
    ctx.fillStyle = 'rgba(255,255,255,0.5)';
    ctx.fillRect(0, 0, 600, 600);
    ctx.font = '40px Fontdiner Swanky';
    ctx.fillStyle = "red";
    ctx.textAlign = "center";
    ctx.fillText(result, c.width / 2, c.height / 2);
    ctx.font = '30px Fontdiner Swanky';
    ctx.fillText("click anywhere to play again!", c.width / 2, c.height / 2 + 60);
}